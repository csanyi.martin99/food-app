package com.example.food_app.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.food_app.R;
import com.example.food_app.model.Food;

import java.util.List;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder>{

    private Context context;
    private List<Food> foodList;

    public FoodAdapter(Context context, List<Food> foodList) {
        this.context = context;
        this.foodList = foodList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.food_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Food food = foodList.get(position);
        holder.bindFood(food);
    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView foodNameTextView;
        private TextView foodPriceTextView;
        private ImageView foodPictureTextView;
        private RelativeLayout foodBgRelativeLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            foodNameTextView = itemView.findViewById(R.id.food_name);
            foodPriceTextView = itemView.findViewById(R.id.food_price);
            foodPictureTextView = itemView.findViewById(R.id.food_image);
            foodBgRelativeLayout = itemView.findViewById(R.id.food_bg);

        }

        public void bindFood(Food paramFood) {
            foodNameTextView.setText(paramFood.getFoodName());
            foodPriceTextView.setText(paramFood.getFoodPrice());
            foodPictureTextView.setImageResource(paramFood.getPictureId());
            foodBgRelativeLayout.setBackgroundResource(paramFood.getBackgroundId());
        }
    }


}
