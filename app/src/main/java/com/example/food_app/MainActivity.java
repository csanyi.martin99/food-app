package com.example.food_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.food_app.controller.FoodAdapter;
import com.example.food_app.model.Food;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button firstButton;
    private Button secondButton;
    private Button thirdButton;
    private Button fourthButton;
    private Button fifthButton;
    private Button sixthButton;
    private Button seventhButton;
    private List<Food> foodList;
    private RecyclerView foodRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       firstButton = findViewById(R.id.firstButton);
       secondButton = findViewById(R.id.secondButton);
       thirdButton = findViewById(R.id.thirdButton);
       fourthButton = findViewById(R.id.fourthButton);
       fifthButton = findViewById(R.id.fifthButton);
       sixthButton = findViewById(R.id.sixthButton);
       seventhButton = findViewById(R.id.seventhButton);

       firstButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (firstButton.isSelected() == true) {
                   firstButton.setSelected(false);
               } else if (firstButton.isSelected() == false) {
                    firstButton.setSelected(true);
               }
           }
       });

        secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (secondButton.isSelected() == true) {
                    secondButton.setSelected(false);
                } else if (secondButton.isSelected() == false) {
                    secondButton.setSelected(true);
                }
            }
        });

        thirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (thirdButton.isSelected() == true) {
                    thirdButton.setSelected(false);
                } else if (thirdButton.isSelected() == false) {
                    thirdButton.setSelected(true);
                }
            }
        });

        fourthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fourthButton.isSelected() == true) {
                    fourthButton.setSelected(false);
                } else if (fourthButton.isSelected() == false) {
                    fourthButton.setSelected(true);
                }
            }
        });

        fifthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fifthButton.isSelected() == true) {
                    fifthButton.setSelected(false);
                } else if (fifthButton.isSelected() == false) {
                    fifthButton.setSelected(true);
                }
            }
        });

        sixthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sixthButton.isSelected() == true) {
                    sixthButton.setSelected(false);
                } else if (sixthButton.isSelected() == false) {
                    sixthButton.setSelected(true);
                }
            }
        });

        seventhButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (seventhButton.isSelected() == true) {
                    seventhButton.setSelected(false);
                } else if (seventhButton.isSelected() == false) {
                    seventhButton.setSelected(true);
                }
            }
        });


        Food food1 = new Food("Rákos lófasz tarhonyával", "$13.00",
                R.drawable.asian_food, R.drawable.radius_skin_color);
        Food food2 = new Food("Sushi tányéron", "$26.50",
                R.drawable.asian_food, R.drawable.radius_blue_color);

        foodList = new ArrayList<>();

        foodList.add(food1);
        foodList.add(food2);

        foodRecyclerView = findViewById(R.id.food_recycler_view);

        //LinearLayoutManager az elemek megjelenítésére szolgál
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        foodRecyclerView.setLayoutManager(linearLayoutManager);

        FoodAdapter foodAdapter = new FoodAdapter(this, foodList);
        foodRecyclerView.setAdapter(foodAdapter);

    }




}