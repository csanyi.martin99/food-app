package com.example.food_app.model;

public class Food {

    private String foodName;
    private String foodPrice;
    private Integer pictureId;
    private Integer backgroundId;

    public Food(String foodName, String foodPrice, Integer pictureId, Integer backgroundId) {
        this.foodName = foodName;
        this.foodPrice = foodPrice;
        this.pictureId = pictureId;
        this.backgroundId = backgroundId;
    }

    public String getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(String foodPrice) {
        this.foodPrice = foodPrice;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public Integer getPictureId() {
        return pictureId;
    }

    public void setPictureId(Integer pictureId) {
        this.pictureId = pictureId;
    }

    public Integer getBackgroundId() {
        return backgroundId;
    }

    public void setBackgroundId(Integer backgroundId) {
        this.backgroundId = backgroundId;
    }
}
